import time

import tensorflow as tf

from ProtoNet.model_funcs import ProtoNetEncoder
from ProtoNet.utils.ImageDataLoader import create_train_dataloader_from_csv, create_test_dataloader_from_csv
from .training_engine import TrainEngine


def train(img_root_dir,
          train_csv_path,
          filepath_column_name,
          label_column_name,
          val_csv_path,
          model_save_path,
          epochs,
          episodes_per_epoch,
          n_shots_train,
          n_way_train,
          n_query_train,
          max_ims_for_val,
          early_stopping_patience,
          use_gpu,
          gpu_num,
          lr,
          target_image_shape,
          do_imagenet_preprocess,
          print_model_summary):

    # Determine device
    if use_gpu is True:
        device = tf.config.experimental.list_logical_devices('GPU')[gpu_num]
        device_name = device.name
    else:
        device_name = tf.config.experimental.list_logical_devices('CPU')[0].name

    # Create DataLoaders for training and validation
    train_loader = create_train_dataloader_from_csv(img_root_dir=img_root_dir,
                                                    train_csv_path=train_csv_path,
                                                    filepaths_column=filepath_column_name,
                                                    labels_column=label_column_name,
                                                    n_shots=n_shots_train,
                                                    n_way=n_way_train,
                                                    n_query=n_query_train,
                                                    target_image_shape=target_image_shape,
                                                    do_imagenet_preprocess=do_imagenet_preprocess)
    if val_csv_path is not None:
        val_loader = create_test_dataloader_from_csv(img_root_dir=img_root_dir,
                                                     test_csv_path=val_csv_path,
                                                     train_csv_path=train_csv_path,
                                                     filepaths_column=filepath_column_name,
                                                     labels_column=label_column_name,
                                                     target_image_shape=target_image_shape,
                                                     do_imagenet_preprocess=do_imagenet_preprocess)
    else:
        val_loader=None

    # Setup model for training
    model = ProtoNetEncoder(image_shape=target_image_shape, return_summary=print_model_summary)
    optimizer = tf.keras.optimizers.Adam(lr=lr)

    # Metrics to gather
    train_loss = tf.metrics.Mean(name='train_loss')
    train_acc = tf.metrics.Mean(name='train_accuracy')
    train_losses, train_accs = [], []

    if val_csv_path is not None:
        val_loss = tf.metrics.Mean(name='val_loss')
        val_acc = tf.metrics.Mean(name='val_accuracy')
        val_losses, val_accs = [], []


    @tf.function
    def proto_loss_func(support, query):
        loss, acc = model(support, query)
        return loss, acc

    @tf.function
    def train_step(loss_func, support, query):
        # Forward and gradient updates
        with tf.GradientTape() as gtape:
            loss, acc = model([support, query], mode='train')
        gradients = gtape.gradient(loss, model.trainable_variables)
        optimizer.apply_gradients(zip(gradients, model.trainable_variables))
        train_loss(loss)
        train_acc(acc)

    if val_csv_path is not None:
        @tf.function
        def val_step(loss_func, support, query):
            loss, acc = model([support, query], mode='val')
            val_loss(loss)
            val_acc(acc)

    train_engine = TrainEngine()

    def on_start(state):
        print('Training commencing')

    train_engine.hooks['on_start'] = on_start

    def on_end(state):
        print('Training complete')

    train_engine.hooks['on_end'] = on_end

    def on_start_epoch(state):
        print(f'\nEpoch {state["epoch"]} started')
        train_loss.reset_states()
        train_acc.reset_states()

        if val_csv_path is not None:
            val_loss.reset_states()
            val_acc.reset_states()

    train_engine.hooks['on_start_epoch'] = on_start_epoch

    def on_end_epoch(state):
        if val_csv_path is not None:
            val_loader = state['val_loader']
            loss_func = state['loss_func']

            vs, vq = val_loader.air_next_episode(org_query_by_class=True, num_support_ims=max_ims_for_val)
            val_step(loss_func, vs, vq)

        print(f'\nEpoch {state["epoch"]} ended')
        epoch = state["epoch"]

        print(f'\nEpoch {epoch}, Loss: {train_loss.result()}, Accuracy: {train_acc.result() * 100}% '
            f'{f", Val. Loss: {val_loss.result()}, Val. Accuracy: {val_acc.result() * 100}%" if val_csv_path is not None else ""}')

        if val_csv_path is not None:
            cur_loss = val_loss.result().numpy()

            if cur_loss < state['best_val_loss']:
                print(f"Saving new best model with validation loss {cur_loss}")
                state['best_val_loss'] = cur_loss
                model.save(model_save_path)

            val_losses.append(cur_loss)
            val_accs.append(val_acc.result().numpy())

        train_losses.append(train_loss.result().numpy())
        train_accs.append(train_acc.result().numpy())

        if early_stopping_patience is not None:
            if val_csv_path is None:
                print("Early stopping requires validation data, and is thus being skipped")
            elif len(val_losses) > early_stopping_patience and min(val_losses[-early_stopping_patience:-1]) <= val_losses[-1]:
                state['early_stopping_triggered'] = True

    train_engine.hooks['on_end_epoch'] = on_end_epoch

    def on_start_episode(state):
        support, query = state['sample']
        loss_func = state['loss_func']
        train_step(loss_func, support, query)

    train_engine.hooks['on_start_episode'] = on_start_episode

    def on_end_episode(state):
        pass

    train_engine.hooks['on_end_episode'] = on_end_episode

    time_start = time.time()
    with tf.device(device_name):
        train_engine.train(
            loss_func=proto_loss_func,
            train_loader=train_loader,
            val_loader=val_loader,
            epochs=epochs,
            n_episodes=episodes_per_epoch)

    time_end = time.time()

    if val_csv_path is None:
        model.save(model_save_path)

    elapsed = time_end - time_start
    h, minutes = elapsed // 3600, elapsed % 3600 // 60
    sec = elapsed - ((h * 3600) + (minutes * 60))
    print(f"Training took {h} hours, {minutes} minutes, {sec} seconds")

    history = {'loss':train_losses, 'accuracy':train_accs}

    if val_csv_path is not None:
        history.update({'val_loss':val_losses,'val_accuracy':val_accs})

    return history