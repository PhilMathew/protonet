import tensorflow as tf
from sklearn.metrics import *
import pandas as pd

from ProtoNet.model_funcs import ProtoNetEncoder
from ProtoNet.utils.ImageDataLoader import create_test_dataloader_from_csv
from ProtoNet.utils.plot_metrics import plot_confmat


def predict_labels(img_root_dir,
                   test_csv_path,
                   train_csv_path,
                   filepath_column_name,
                   label_column_name,
                   max_ims_for_protos,
                   model_save_path,
                   use_gpu,
                   gpu_num,
                   target_image_shape,
                   do_imagenet_preprocess,
                   print_model_summary):

    # Determine device
    if use_gpu is True:
        device = tf.config.experimental.list_logical_devices('GPU')[gpu_num]
        device_name = device.name
    else:
        device_name = tf.config.experimental.list_logical_devices('CPU')[0].name

    # Create a DataLoader for testing
    test_loader = create_test_dataloader_from_csv(img_root_dir=img_root_dir,
                                                  test_csv_path=test_csv_path,
                                                  train_csv_path=train_csv_path,
                                                  filepaths_column=filepath_column_name,
                                                  labels_column=label_column_name,
                                                  target_image_shape=target_image_shape,
                                                  do_imagenet_preprocess=do_imagenet_preprocess)

    # Setup model for training
    model = ProtoNetEncoder(image_shape=target_image_shape, return_summary=print_model_summary)
    model.load(str(model_save_path))

    def predict_func(support, query, classes_used):
        pred_labels = model([support, query, classes_used], mode='test')
        return pred_labels


    with tf.device(device_name):
        support, query, classes_used = test_loader.air_next_episode(return_class_map=True, num_support_ims=max_ims_for_protos)
        pred_labels, prototypes = predict_func(support, query, classes_used)

    return pred_labels

def evaluate_model(img_root_dir,
                   test_csv_path,
                   train_csv_path,
                   filepath_column_name,
                   label_column_name,
                   max_ims_for_protos,
                   model_save_path,
                   use_gpu,
                   gpu_num,
                   target_image_shape,
                   do_imagenet_preprocess,
                   print_model_summary):

    pred_labels = predict_labels(img_root_dir,
                                 test_csv_path,
                                 train_csv_path,
                                 filepath_column_name,
                                 label_column_name,
                                 max_ims_for_protos,
                                 model_save_path,
                                 use_gpu,
                                 gpu_num,
                                 target_image_shape,
                                 do_imagenet_preprocess,
                                 print_model_summary)

    true_labels = list(pd.read_csv(test_csv_path)[label_column_name])

    plot_confmat(true_labels, pred_labels, confmat_name="confmat")

    report = pd.DataFrame(classification_report(true_labels, pred_labels, output_dict=True))

    print(f"Test Accuracy: {accuracy_score(true_labels, pred_labels)}")

    return report




