import numpy as np
import tensorflow as tf


def euclidean_dist(x, y):
    """
    Calculate the distances between two tensors
    """
    n = x.shape[0]
    m = y.shape[0]
    x = tf.tile(tf.expand_dims(x, 1), [1, m, 1])
    y = tf.tile(tf.expand_dims(y, 0), [n, 1, 1])
    return tf.reduce_mean(tf.math.pow(x - y, 2), 2)


class ProtoNetEncoder(tf.keras.Model):
    """
    Class implementing prototypical net
    """

    def __init__(self, image_shape, return_summary=False):
        super(ProtoNetEncoder, self).__init__()
        self.w, self.h, self.c = image_shape[0], image_shape[1], image_shape[2]

        self.encoder = tf.keras.models.Sequential([  # defines the model architecture
            tf.keras.layers.InputLayer(input_shape=image_shape),
            tf.keras.layers.Conv2D(filters=64, kernel_size=3, padding='same'),
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.ReLU(),
            tf.keras.layers.MaxPool2D((2, 2)),
            # tf.keras.layers.MaxPool2D((4, 4)),

            tf.keras.layers.Conv2D(filters=64, kernel_size=3, padding='same'),
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.ReLU(),
            tf.keras.layers.MaxPool2D((2, 2)),
            # tf.keras.layers.MaxPool2D((4, 4)),

            tf.keras.layers.Conv2D(filters=64, kernel_size=3, padding='same'),
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.ReLU(),
            tf.keras.layers.MaxPool2D((2, 2)),
            # tf.keras.layers.MaxPool2D((4, 4)),

            tf.keras.layers.Conv2D(filters=64, kernel_size=3, padding='same'),
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.ReLU(),
            tf.keras.layers.MaxPool2D((2, 2)),
            # tf.keras.layers.MaxPool2D((4, 4)),

            tf.keras.layers.Flatten()
        ])

        if return_summary:
            self.encoder.summary()

    def _compute_logits(self, support, query, mode, return_prototypes=False):
        """
        Calculates logits for a single episode
        :param support: support set
        :param query: query set
        :param mode: whether to run in in 'train', 'val', or 'test' mode
        :param return_prototypes: Whether or not to return the prototypes used in this episode
        :return: logits for the data. If training is true, this will also return the fabricated labels for use in loss and accuracy calculations
        """
        if mode is not 'train' and mode is not 'val' and mode is not 'test':
            raise ValueError(f"mode must be \"train\", \"val\", or \"test\", not \"{mode}\"")

        num_class = support.shape[0]
        num_support = support.shape[1]
        num_query = query.shape[1] if mode is not 'test' else query.shape[0]

        # In the following 2 lines, labels are formed to compute loss and acc at end
        # (NOTE: THESE ARE NOT THE ACTUAL LABELS FOR THE DATA. Data with the same label here will have the same ground
        # truth labels, but could be referred to by a different number)
        y = np.tile(np.arange(num_class)[:, np.newaxis], (1, num_query))
        y_onehot = tf.cast(tf.one_hot(y, num_class), tf.float32)

        if mode is 'train' or mode is 'val':
            data_to_feed = tf.concat([tf.reshape(support, shape=(num_class * num_support, self.w, self.h, self.c)),
                tf.reshape(query, shape=(num_class * num_query, self.w, self.h, self.c))], axis=0)
        else:
            data_to_feed = tf.concat([tf.reshape(support, shape=(num_class * num_support, self.w, self.h, self.c)),
                                      query], axis=0)

        enc_out = self.encoder(data_to_feed)

        emb_support, emb_query = tf.reshape(enc_out[:num_class * num_support],
                                            shape=(num_class, num_support, tf.shape(enc_out)[-1])), \
                                 enc_out[num_class * num_support:]

        prototypes = tf.reduce_mean(emb_support, axis=1)

        dists = euclidean_dist(emb_query, prototypes)

        if mode is 'train' or mode is 'val':
            log_p_y = tf.reshape(tf.nn.log_softmax(-dists, axis=-1),
                                 shape=(num_class, num_query, -1))
        else:
            log_p_y = tf.nn.log_softmax(-dists, axis=-1)

        return_list = [log_p_y]

        if mode is 'train' or mode is 'val':
            return_list.append(y_onehot)
            return_list.append(y)

        if return_prototypes:
            return_list.append(prototypes)

        if len(return_list) < 2: # TODO Find a way to phase this thing out
            return log_p_y
        else:
            return return_list

    def _calc_loss_and_acc(self, support, query, mode):
        """
        Calculates loss and accuracy for model
        :param support: support set
        :param query: query set
        :param mode: whether to run in in 'train' or 'val'
        :return: loss and accuracy
        """
        logits, y_onehot, y = self._compute_logits(support, query, mode, return_prototypes=False)

        loss = -tf.reduce_mean(tf.reshape(tf.reduce_sum(tf.multiply(y_onehot, logits), axis=-1), [-1]))

        eq = tf.cast(tf.equal(
            tf.cast(tf.argmax(logits, axis=-1), tf.int32),
            tf.cast(y, tf.int32)), tf.float32)

        acc = tf.reduce_mean(eq)

        return loss, acc

    def _compute_logits_for_testing_w_precomputed_prototypes(self, query, prototypes):
        """
        Computes logits for testign given that prototypes are specified
        :param query: test data
        :param prototypes: prototypes to use
        :return: logits based on these prototypes
        """
        emb_query = self.encoder(query)

        dists = euclidean_dist(emb_query, prototypes)

        log_p_y = tf.nn.log_softmax(-dists, axis=-1)

        return log_p_y

    def call(self, inputs, mode=None, mask=None, use_precomputed_prototypes=False):
        """
        Calls the model to run
        :param inputs: input values for each mode. THIS MUST BE A LIST STRUCTURED AS FOLLOWS: [support, query], [support, query, class_map], or [query, prototypes, class_map]
        :param mode: whether to run in in 'train', 'val', or 'test'
        :param mask: A mask or list of masks. A mask can be either a tensor or None (no mask).
        :param use_precomputed_prototypes: whether to do computations with precomputed prototypes
        """
        if mode is 'train' or mode is 'val':
            support, query = inputs[0], inputs[1]
            return self._calc_loss_and_acc(support, query, mode)
        elif use_precomputed_prototypes:
            query, prototypes, class_map = inputs[0], inputs[1], inputs[2]

            log_p_y = self._compute_logits_for_testing_w_precomputed_prototypes(query, prototypes)
            y_pred = [class_map[i] for i in tf.argmax(log_p_y, axis=-1).numpy()]

            return y_pred, prototypes
        else:
            support, query, class_map = inputs[0], inputs[1], inputs[2] # class_map should be a list of the class labels ordered the SAME as the training data
                                                                        # (e.g. if support is shaped (5, N, W, H, C), then class_map should be the labels for each of the five classes in the order they were supplied)
            log_p_y, prototypes = self._compute_logits(support, query, mode='test', return_prototypes=True)
            y_pred = [class_map[i] for i in tf.argmax(log_p_y, axis=-1).numpy()]

            return y_pred, prototypes

    def save(self, filepath, overwrite=True, include_optimizer=True, save_format=None, signatures=None, options=None):
        """
        Save encoder to the file.
        :param model_path: path to the .h5 file.
        """
        self.encoder.save(filepath=filepath, overwrite=overwrite, include_optimizer=include_optimizer)

    def load(self, model_path):
        """
        Load encoder from the file.
        :param model_path: path to the .h5 file.
        """
        self.encoder(tf.zeros([1, self.w, self.h, self.c]))
        self.encoder.load_weights(model_path)
