from pathlib import Path
import numpy as np

from ProtoNet.model_evaluation.setup_eval import predict_labels, evaluate_model
from ProtoNet.model_training.setup_training import train
from ProtoNet.utils.plot_metrics import plot_training_metrics


class PrototypicalNetwork:
    def __init__(self, root_run_dir, run_desc, img_root_dir, filepath_column_name,
                 label_column_name, model_save_path='auto', plot_training_metrics=True, use_gpu=True, gpu_num=0,
                 target_image_shape=(224, 224, 3),
                 do_imagenet_preprocess=True):
        """
        Wrapper class for protonet functions
        :param root_run_dir:
        :param run_desc:
        :param img_root_dir:
        :param filepath_column_name: name of column with image paths
        :param label_column_name: name of column with image labels
        :param model_save_path: path to save model weights to (defaults to 'auto', which saves teh weights file to the run directory)
        :param plot_training_metrics:
        :param use_gpu: whether to train using a g (defaults to True)
        :param gpu_num: CUDA number of the GPU to be used in training (defaults to 0)
        :param target_image_shape: shape to resize images to (defaults to (224, 224, 3))
        :param do_imagenet_preprocess: whether to preprocess images using imagenet preprocessing (defaults to true)
        """
        self.root_run_dir = Path(root_run_dir)
        self.model_working_dir = self.root_run_dir / run_desc
        self.plot_training_metrics = plot_training_metrics
        self.img_root_dir = img_root_dir
        self.filepath_column_name = filepath_column_name
        self.label_column_name = label_column_name
        self.use_gpu = use_gpu
        self.gpu_num = gpu_num
        self.target_image_shape = target_image_shape
        self.do_imagenet_preprocess = do_imagenet_preprocess

        if not self.model_working_dir.exists():
            self.model_working_dir.mkdir()

        if model_save_path is 'auto':
            self.model_save_path = self.model_working_dir / 'encoder.h5'
        else:
            self.model_save_path = model_save_path

    def fit(self,
            train_csv_path,
            val_csv_path,
            epochs=1000,
            episodes_per_epoch=20,
            n_shots_train=5,
            n_way_train=2,
            n_query_train=20,
            max_ims_for_val=None,
            early_stopping_patience=None,
            lr=.001,
            print_model_summary=True):
        """
        Training function for prototypical network
        :param train_csv_path: path to csv containing training data
        :param val_csv_path: path to csv containing validation data
        :param epochs: number of epochs to train for (defaults to 1000)
        :param episodes_per_epoch: number of episodes to run per epoch (defaults to 20)
        :param n_shots_train: number of support examples per class per training episode (defaults to 5)
        :param n_way_train: number of classes to use per training episode (defaults to 2)
        :param n_query_train: number of query examples per class per training episode (defaults to 20)
        :param max_ims_for_val: max number of images per class to use for computing prototypes in validation. If None this uses all available data (defaults to None).
        :param early_stopping_patience: how many epochs to wait before early stopping* kicks in (defaults to 25)
        :param lr: initial learning rate (defaults to .001)
        :param print_model_summary: whether to print out the model's architecture (defaults to true)
        :return: dictionary of loss and accuracy as per epoch for training and validation
        """

        history = train(self.img_root_dir,
                        train_csv_path,
                        self.filepath_column_name,
                        self.label_column_name,
                        val_csv_path,
                        self.model_save_path,
                        epochs,
                        episodes_per_epoch,
                        n_shots_train,
                        n_way_train,
                        n_query_train,
                        max_ims_for_val,
                        early_stopping_patience,
                        self.use_gpu,
                        self.gpu_num,
                        lr,
                        self.target_image_shape,
                        self.do_imagenet_preprocess,
                        print_model_summary)

        if self.plot_training_metrics:
            plot_training_metrics(history, metrics_save_path=self.model_working_dir)

        return history

    def evaluate(self,
                 test_csv_path,
                 train_csv_path,
                 max_ims_for_protos=None,
                 save_metrics=True,
                 print_model_summary=False):
        """
        Evaluates the model on testing data
        :param test_csv_path: path to csv containing testing data
        :param train_csv_path: path to csv containing training data
        :param max_ims_for_protos: max number of images per class to use for computing prototypes. If None this uses all available data (defaults to None).
        :param save_metrics: whether to save the evaluation metrics (defaults to True)
        :param print_model_summary: whether to print out the model's architecture (defaults to false)
        :return: sklearn classification report for the model, and saves a confusion matrix
        """
        report = evaluate_model(self.img_root_dir,
                                test_csv_path,
                                train_csv_path,
                                self.filepath_column_name,
                                self.label_column_name,
                                max_ims_for_protos,
                                self.model_save_path,
                                self.use_gpu,
                                self.gpu_num,
                                self.target_image_shape,
                                self.do_imagenet_preprocess,
                                print_model_summary)

        if save_metrics:
            report.to_csv(str(self.model_working_dir / 'classification_report.csv'))

        return report

    def predict(self,
                test_csv_path,
                train_csv_path,
                max_ims_for_protos=None,
                save_preds = False,
                print_model_summary=False):
        """
        Predicts labels of  testing data
        :param test_csv_path: path to csv containing testing data
        :param train_csv_path: path to csv containing training data
        :param max_ims_for_protos: max number of images per class to use for computing prototypes. If None this uses all available data (defaults to None).
        :param save_preds: whether to save the predicted labels (defaults to True)
        :param print_model_summary: whether to print out the model's architecture (defaults to false)
        :return: various evaluation metrics for the given model
        """
        pred_labels = predict_labels(self.img_root_dir,
                                     test_csv_path,
                                     train_csv_path,
                                     self.filepath_column_name,
                                     self.label_column_name,
                                     max_ims_for_protos,
                                     self.model_save_path,
                                     self.use_gpu,
                                     self.gpu_num,
                                     self.target_image_shape,
                                     self.do_imagenet_preprocess,
                                     print_model_summary)

        if save_preds:
            np.savez(self.model_working_dir / 'pred_labels.npz', pred_labels=pred_labels)

        return pred_labels