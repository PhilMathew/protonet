import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from sklearn.metrics import confusion_matrix


def plot_confmat(true_labels, pred_labels, confmat_name):
    """
    Plots a confusion matrix
    :param true_labels: ground truth labels for data
    :param pred_labels: predicted labels from model
    :param confmat_name: name of the confusion matrix
    """
    fig2, ax = plt.subplots(1, 1, num=2)

    cm = confusion_matrix(true_labels, pred_labels)
    cm_norm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]  # normalize the confusion matrix

    annot = np.zeros_like(cm, dtype=object)
    for i in range(annot.shape[0]):  # Creates an annotation array for the heatmap
        for j in range(annot.shape[1]):
            annot[i][j] = f'{cm[i][j]}\n{round(cm_norm[i][j] * 100, ndigits=3)}%'

    ax = sns.heatmap(cm_norm, annot=annot, fmt='', cbar=True, cmap=plt.cm.magma, vmin=0, ax=ax)  # plot the confusion matrix

    ax.set(ylabel='Predicted Label', xlabel='Actual Label')

    fig2.savefig(f'{confmat_name}.png')  # save the confusion matrix

def plot_training_metrics(history, metrics_save_path): # TODO Fix this to work nicely without val metrics
    """
    Plot the accuracy and loss for training and validation during the training cycle
    :param history: dictionary with metrics
    :param metrics_save_path: path to save the plots to
    """
    fig, [[train_acc, train_loss], [val_acc, val_loss]] = plt.subplots(nrows=2, ncols=2)

    train_acc.plot(history['accuracy'])  # training accuracy
    train_acc.set_title('Training Accuracy')

    train_loss.plot(history['loss'])  # training loss
    train_loss.set_title('Training Loss')

    if 'val_accuracy' in history.keys() and 'val_loss' in history.keys():
        val_acc.plot(history['val_accuracy'])  # validation accuracy
        val_acc.set_title('Validation Accuracy')

        val_loss.plot(history['val_loss'])  # validation loss
        val_loss.set_title('Validation Loss')
    else:
        fig.delaxes(val_loss)
        fig.delaxes(val_acc)

    fig.tight_layout()  # prevents overlapping plots
    fig.savefig(metrics_save_path / 'training_metrics.png')  # saves all 4 plots

