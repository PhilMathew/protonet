from pathlib import Path

import cv2
from skimage.io import imread
from skimage.color import gray2rgb
from skimage.transform import resize
import numpy as np
import pandas as pd
from keras.applications.imagenet_utils import preprocess_input


def load_images(im_paths, target_size=(224, 224, 3), do_imagenet_preprocess=True):
    """
    Loads in an array of images
    :param im_paths: paths to images
    :param target_size: size for resize images to (defaults to (224, 224, 3)
    :param do_imagenet_preprocess: whether or not image should be preprocessed with imagenet's preprocessing method (defaults to True)
    :return: numpy array of images
    """
    img_arr = []

    for im_path in im_paths:
        try:
            try: # try with cv2
                im = cv2.cvtColor(cv2.imread(im_path), cv2.COLOR_BGR2RGB)
                if target_size is not None:
                    im = cv2.resize(im, target_size[0:2])
            except Exception as e: # fall back to skimage
                im = imread(im_path)
                im = gray2rgb(im, alpha=False)
                if im.ndim != 3 or im.shape[2] != 3:
                    raise
                if target_size is not None:
                    im = resize(im, target_size, preserve_range=True)
            img_arr.append(im)
        except Exception as e: # in case both methods of reading the image fail
            raise Exception(f"Invalid image - {im_path}") from e

    if do_imagenet_preprocess:
        return preprocess_input(np.asarray(img_arr))
    else:
        return np.asarray(img_arr, dtype=int)


class TrainDataLoader:
    """
    Constructs support and query sets for each episode of training
    """

    def __init__(self, im_paths, labels, num_class_per_ep, num_support, num_query, target_shape, do_imagenet_preprocess):
        """
        Initializes the DataLoader
        :param im_paths: paths to the images to be used
        :param labels: labels of the images to be used
        :param num_class_per_ep: number of classes to use per episode
        :param num_support: number of support examples per class per episode
        :param num_query: number of query examples per class per episode
        :param target_shape: shape to resize images to
        :param do_imagenet_preprocess: whether to apply imagenet preprocessing to the data
        """
        self.im_paths = im_paths
        self.labels = labels
        self.classes = np.unique(self.labels)
        self.num_class_per_ep = num_class_per_ep
        self.num_support = num_support
        self.num_query = num_query
        self.w, self.h, self.c = target_shape
        self.do_imagenet_preprocess = do_imagenet_preprocess

    def air_next_episode(self):
        """
        Constructs the support and query sets of images to use in the next episode
        :return: support and query sets, and the classes used in this episode if for_eval is True
        """
        num_examples = np.min(np.unique(self.labels, return_counts=True)[1]) # determine the minimum number of images to use per episode (can't be more than the number of images that correspond to the class with the least images)

        if num_examples < self.num_support + self.num_query: # num_support = number of shots, num_query = batch size
            raise ValueError(
                f"Number of required examples is too high, please reduce the number of support and/or query examples as"
                f" needed ({num_examples} examples available, {self.num_support + self.num_query} examples requested)")

        # create arrays to store images
        support = np.zeros(shape=(self.num_class_per_ep, self.num_support, self.w, self.h, self.c), dtype=np.float32)
        query = np.zeros(shape=(self.num_class_per_ep, self.num_query, self.w, self.h, self.c), dtype=np.float32)

        classes_ep = np.random.permutation(self.classes)[:self.num_class_per_ep] # randomly sample classes to use in this episode

        for i, cls in enumerate(classes_ep):
            usable_paths = [path for p_idx, path in enumerate(self.im_paths) if self.labels[p_idx] == cls] # construct a list of paths that correspond to the desired label
            ims_to_use = np.random.permutation(num_examples)[:self.num_support + self.num_query] # randomly sample the set to obtain images for this episode

            # The next 5 lines partition the images into support and query sets
            support[i] = load_images(im_paths=[usable_paths[idx] for idx in ims_to_use[:self.num_support]],
                                     target_size=(self.w, self.h, self.c), do_imagenet_preprocess=self.do_imagenet_preprocess)
            query[i] = load_images(
                im_paths=[usable_paths[idx] for idx in ims_to_use[self.num_support:self.num_support + self.num_query]],
                target_size=(self.w, self.h, self.c), do_imagenet_preprocess=self.do_imagenet_preprocess)

        return support, query


class TestDataLoader:
    def __init__(self, train_im_paths, test_im_paths, train_labels, test_labels, target_shape, do_imagenet_preprocess):
        """
        Initializes a data loader for val or test data
        :param train_im_paths: paths to training images (used to create prototypes
        :param test_im_paths: paths to testing images (for inference)
        :param train_labels: labels for the training images
        :param test_labels: labels for the testing images
        :param target_shape: shape to resize images to
        :param do_imagenet_preprocess: whether to apply imagenet preprocessing to the data
        """
        self.train_im_paths, self.train_labels = train_im_paths, train_labels
        self.test_im_paths, self.test_labels = test_im_paths, test_labels
        self.classes = np.unique(self.train_labels)
        self.w, self.h, self.c = target_shape
        self.do_imagenet_preprocess = do_imagenet_preprocess

    def air_next_episode(self, return_class_map=False, org_query_by_class=False, num_support_ims=None):
        """
        Constructs the next episode for validation or testing
        :param return_class_map: whether to return the query labels along with the support and query sets (defaults to False)
        :param org_query_by_class: whether to organize the query set by class (defaults to False)
        :param num_support_ims: limit on the number of images per class used to make prototypes. If None this will use all available data. (defaults to None)
        :return: support and query sets, and the class map if return_class_map is True
        """
        min_support_ims_per_class = num_support_ims if num_support_ims is not None else np.min(np.bincount(self.train_labels))

        support = np.zeros(shape=(self.classes.shape[0], min_support_ims_per_class, self.w, self.h, self.c))

        if not org_query_by_class:
            query = load_images(im_paths=self.test_im_paths, target_size=(self.w, self.h, self.c),
                                do_imagenet_preprocess=self.do_imagenet_preprocess)
        else:
            min_query_ims_per_class = np.min(np.bincount(self.test_labels))

            if min_query_ims_per_class != len(self.test_labels) // 2:
                print(f"Validation set found to be unbalanced, model will thus use balanced subsets of "
                      f"{min_query_ims_per_class} images per class for validation.") # TODO Fix this to work on unbalanced validation data

            query = np.zeros(shape=(self.classes.shape[0], min_query_ims_per_class, self.w, self.h, self.c))

        for i, cls in enumerate(self.classes):
            usable_paths = [path for p_idx, path in enumerate(self.train_im_paths) if self.train_labels[p_idx] == cls][:min_support_ims_per_class] # construct a list of paths that correspond to the desired label

            # add images to the support set
            support[i] = load_images(im_paths=usable_paths, target_size=(self.w, self.h, self.c),
                                     do_imagenet_preprocess=self.do_imagenet_preprocess)
            if org_query_by_class:
                query[i] = load_images(im_paths=[self.test_im_paths[i] for i, label in enumerate(self.test_labels) if label == cls], target_size=(self.w, self.h, self.c),
                                do_imagenet_preprocess=self.do_imagenet_preprocess)


        if return_class_map:
            return support, query, self.classes
        else:
            return support, query


def create_train_dataloader_from_csv(img_root_dir,
                                     train_csv_path,
                                     filepaths_column,
                                     labels_column,
                                     n_shots,
                                     n_way,
                                     n_query,
                                     target_image_shape,
                                     do_imagenet_preprocess):
    """
    Creates a TrainDataLoader for the specified csv
    :param img_root_dir: root directory for all image data specified in csv
    :param train_csv_path: path to the csv containing the desired training data and labels
    :param filepaths_column: column name in csv for the filepaths
    :param labels_column: column name in csv for labels
    :param n_shots: number of support examples per episode
    :param n_way: number of classes used per episode
    :param n_query: number of query examples per episode
    :param target_image_shape: desired shape of image data
    :param do_imagenet_preprocess: whether or not to use imagenet preprocessing on the images
    :return: an instance of a TrainDataLoader for the train csv
    """
    data_csv = pd.read_csv(train_csv_path)
    im_paths = [str(img_root_dir / Path(path)) for path in data_csv[filepaths_column]]
    labels = list(data_csv[labels_column])

    num_classes_per_ep, num_support, num_query = n_way, n_shots, n_query

    loader = TrainDataLoader(im_paths, labels, num_classes_per_ep, num_support,
                        num_query, target_image_shape, do_imagenet_preprocess)

    return loader


def create_test_dataloader_from_csv(img_root_dir,
                                    test_csv_path,
                                    train_csv_path,
                                    filepaths_column,
                                    labels_column,
                                    target_image_shape,
                                    do_imagenet_preprocess):
    """
    Creates a TestDataLoader for the specified csv
    :param img_root_dir: root directory for all image data specified in csvs
    :param test_csv_path: path to csv of test data
    :param train_csv_path: path to the csv containing the desired training data and labels
    :param filepaths_column: column name in csv for the filepaths
    :param labels_column: column name in csv for labelst
    :param target_image_shape: desired shape of image data
    :param do_imagenet_preprocess: whether or not to use imagenet preprocessing on the images
    :return: an instance of a TestDataLoader for the specified csv
    """
    test_csv = pd.read_csv(test_csv_path)
    train_csv = pd.read_csv(train_csv_path)

    train_im_paths, train_labels = [str(img_root_dir / Path(path)) for path in train_csv[filepaths_column]], list(train_csv[labels_column])
    test_im_paths, test_labels = [str(img_root_dir / Path(path)) for path in test_csv[filepaths_column]], list(test_csv[labels_column])

    loader = TestDataLoader(train_im_paths, test_im_paths, train_labels,
                            test_labels, target_image_shape, do_imagenet_preprocess)

    return loader