from setuptools import setup, find_packages

setup(
    name='ProtoNet',
    url='https://PhilMathew@bitbucket.org/PhilMathew/protonet.git',
    author='Philip Mathew',
    author_email='philipmathew101@gmail.com',
    packages=find_packages(),
    install_requires=[req.replace('==', '>=') for req in open("./requirements.txt").read().splitlines()],
    version='0.1.3',
    description='TensorFlow implementation of Prototypical Networks',
    long_description=open('README.md').read(),
)
