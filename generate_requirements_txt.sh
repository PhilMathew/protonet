#!/usr/bin/env bash

pip freeze > requirements.txt
echo "Don't forget to manually remove sklearn entry from requirements.txt!"
